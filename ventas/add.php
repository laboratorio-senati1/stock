<?php
require_once '../header.php'; 
require_once('../conexion.php');

// Receber o id e a descripcion de todos os productos para para popular a combo
$sql ='SELECT id,descripcion FROM productos;';
$stmt = $pdo->prepare($sql);
$stmt ->execute();
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Pegar o maior id de ventas e somar mais 1 para mostrar no campo id, que é somente leitura
$sqlv ='SELECT MAX(id) AS id FROM ventas;';
$stmtv = $pdo->prepare($sqlv);
$stmtv ->execute();
$venta_id = $stmtv->fetchAll(PDO::FETCH_ASSOC);
$venta_id = $venta_id[0]['id'] + 1;
?>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <div class="b_compra"><h3>Compras</h3> </div>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>ID</td><td><input type="text" name="id" value="<?=$venta_id?>" readonly></td></tr>
            <tr><td><b>producto</td>
            <td>
            <select name="producto_id" id="producto_id">
                <option value="" selected>Selecione</option>
                <?php foreach($data as $row) : ?>
                    <option value="<?= $row['id']; ?>"><?= $row['descripcion']; ?></option>
                <?php endforeach ?>
            </select>
            </td>
            </tr>
            <tr><td><b>cantidad</td><td><input type="text" name="cantidad"></td></tr>
            <tr><td><b>precio</td><td><input type="text" name="precio"></td></tr>
            <tr><td><b>fecha</td><td><input type="text" name="data"></td></tr>
            
            <tr>
                <td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Registrar">&nbsp;&nbsp;&nbsp;
                    <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Regresar">
                </td>
            </tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php
if(isset($_POST['enviar'])){
    // Receber valores digitados no form
    $producto_id = $_POST['producto_id'];
    $cantidad = $_POST['cantidad'];
    $data = $_POST['data'];
    $precio = $_POST['precio'];

    // Pegar stock_minimo de productos para $producto_id
    $sql ='SELECT stock_minimo AS minimo FROM productos WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $min = $stmt->fetch(PDO::FETCH_ASSOC);
    $min = $min['minimo'];

    // Pegar a cantidad em stock para $producto_id
    $sql ='SELECT sum(cantidad) AS cantidad FROM stock WHERE id = '.$producto_id;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $soma_stock = $stmt->fetch(PDO::FETCH_ASSOC);
    $soma_stock = $soma_stock['cantidad'];
    
    if(is_null($soma_stock)) {
        $soma_stock = 0;
    }

    if($cantidad > $soma_stock) {
    ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>  
        <script>
          //  document.getElementById("olamundo").addEventListener("click", exibeMensagem);
            Swal.fire({
              icon: 'error',
              title: 'cantidad muito alta',
              text: 'O stock máximo é de '+"<?=$soma_stock?>",
            })
        // https://blog.betrybe.com/desenvolvimento-web/sweetalert/
        </script>
    <?php
    }else{
        try{
           $sql = "INSERT INTO ventas(producto_id, cantidad, data, precio) VALUES (?,?,?,?)";
           $stm = $pdo->prepare($sql)->execute([$producto_id, $cantidad, $data, $precio]);
           $quant_form = $cantidad;

           if($soma_stock == 0){
               // Adicionar a cantidad comprada ao stock
               $sqle = "INSERT INTO stock(producto_id, cantidad) VALUES (?,?)";
               $stme = $pdo->prepare($sqle)->execute([$producto_id, $cantidad]);
           }else{
               // Atualizar a cantidad comprada no stock
               $stock_atual = $soma_stock - $quant_form;

               $sqle = "UPDATE stock set cantidad = $stock_atual WHERE producto_id = ?";
               $stme = $pdo->prepare($sqle)->execute([$producto_id]);     
           }

           if($stm){
               echo 'Dados inseridos com sucesso';
         header('location: ../stock/index.php');
           }
           else{
               echo 'Erro ao inserir os dados';
           }
       }
       catch(PDOException $e){
          echo $e->getMessage();
       }
    }
}

require_once('../footer.php');
?>
<style>
    body{
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url("https://i.pinimg.com/474x/f6/84/7e/f6847e1064dedfa7a967e6bdb9929f78.jpg");
        opacity: 0.6;
    }
    .jumbotron{
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url("https://i.vimeocdn.com/video/815688397-33636be59ee03b48b6546e070922847b485def8f29739e1af6e7f0d24a066993-d_640x360.jpg");
        color: white;
    }
    .table-bordered{
        margin: auto;
        padding: 10px;
		border: none;
		background-color: #ffffff;
		margin-bottom: 15px;
		font-size: 16px;
        box-shadow: 0 0 10px rgb(255, 255, 255);

    }
    .b_compra{
        color: white;
        text-align: center;
        background-color: black;
        width: 18%;
        margin-left: 40%;
        box-shadow: 0 0 10px rgb(255, 255, 255);
    } 
</style>
