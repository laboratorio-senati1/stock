<?php require_once '../header_idx.php'; ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <div class="b_producto"> <h3>productos</h3> </div>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>Descripcion</td><td><input type="text" name="descripcion"></td></tr>
            <tr><td><b>stock mínimo</td><td><input type="text" name="stock_minimo"></td></tr>
            <tr><td><b>stock máximo</td><td><input type="text" name="stock_maximo"></td></tr>
            <tr><td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Enviar">&nbsp;&nbsp;&nbsp;
            <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Regresar"></td></tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php

if(isset($_POST['enviar'])){
    $descripcion = $_POST['descripcion'];
    $stock_minimo = $_POST['stock_minimo'];
    $stock_maximo = $_POST['stock_maximo'];

    require_once('../conexion.php');
    try{
       $sql = "INSERT INTO productos(descripcion,stock_minimo,stock_maximo) VALUES (?, ?, ?)";
       $stm = $pdo->prepare($sql)->execute([$descripcion, $stock_minimo, $stock_maximo]);;
 
       if($stm){
           echo 'Datos insertados correctos';
     header('location: index.php');
       }
       else{
           echo 'Error al insertar los datos';
       }
   }
   catch(PDOException $e){
      echo $e->getMessage();
   }
}
require_once('../footer.php');
?>
<style>
    body{
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url("https://i.pinimg.com/474x/f6/84/7e/f6847e1064dedfa7a967e6bdb9929f78.jpg");
        opacity: 0.6;
    }
    .jumbotron{
        background-repeat: no-repeat;
        background-size: cover;
        background-image: url("https://i.vimeocdn.com/video/815688397-33636be59ee03b48b6546e070922847b485def8f29739e1af6e7f0d24a066993-d_640x360.jpg");
        color: white;
    }
    .table-bordered{
        margin: auto;
        padding: 10px;
		border: none;
		background-color: #ffffff;
		margin-bottom: 15px;
		font-size: 16px;
        box-shadow: 0 0 10px rgb(255, 255, 255);

    }
    .b_producto{
        color: white;
        text-align: center;
        background-color: black;
        width: 22%;
        margin-left: 40%;
        box-shadow: 0 0 10px rgb(255, 255, 255);
    } 
</style>
