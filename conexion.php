<?php
$hostname = "localhost";
$username = "root";
$password = "admin";
$database = "stock";
$row_limit = 8;
$sgbd = 'mysql'; // mysql, pgsql

// conexion to mysql
try {
    $pdo = new PDO($sgbd.":host=$hostname;dbname=$database", $username, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo"Conexion estable";
} catch (PDOException $err) {
    echo"Fallo en la conexion de base de datos";
    die("Error! " . $err->getMessage());
}
